beacons:
  inotify:
    /srv/team:
      mask:
        - create
        - modify
        - delete
      recurse: True
      auto_add: True

    interval: 1
    disable_during_state_run: True