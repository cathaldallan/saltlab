{% set length = salt['pillar.get']('sleep', 10) %}
test sleep for {{ length }} seconds:
  module.run:
    - name: test.sleep
    - length: {{ length }}
