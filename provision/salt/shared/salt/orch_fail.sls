# TODO run this orchestration from the CCM, targeting the LCM
# TODO tell LCM to restart after highstate
# TODO wait for the minion start event before continuing with orchestration

Step01:
  salt.function:
    - tgt: '*'
    - name: test.ping

Step02:
  salt.state:
    - tgt: '*'
    - highstate: True

Step03:
  salt.state:
    - tgt: '*'
    - sls:
      - test.sleep
    - ret: json
    - pillar:
        sleep: {{ salt['pillar.get']('sleep', 5) }}

Step04:
  salt.state:
    - tgt: '*'
    - sls:
      - test.fail_without_changes

Step05:
  salt.function:
    - tgt: '*'
    - name: cmd.run
    - kwarg:
        cmd: 'date'
