"""
This is custom execution module B
"""

import logging

__virtualname__ = "cmod"


def __virtual__():    
    if 'central-cm' in __salt__["grains.get"]("roles", []):
        return __virtualname__
    else:
        return False, "Module only available on central configuration managers."


def _private():
    """
    Private function example.
    """
    return {"private": "value"}


def public(a, b):
    """
    Public function example.

    Args:
        a: A description
        b: B description
    """
    return {
        "a": a,
        "b": b,
        "c": a + b
    }
