"""
This is custom execution module A
"""

import logging

__virtualname__ = "cmod"


def __virtual__():    
    if 'local-cm' in __salt__["grains.get"]("roles", []):
        return __virtualname__
    else:
        return False, "Module only available on local-cm configuration managers."


def _private():
    """
    Private function example.
    """
    return {"private": "value"}

def public(a, b):
    """
    Public function example.
    Args:
        a: A description
        b: B description
    """
    return {
        "a": a,
        "b": b,
    }

def fail():
    """This function will always fail."""
    1/0
