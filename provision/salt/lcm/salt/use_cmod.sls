test cmod state module succeeds:
  cmod.test_succeeded:
    - a: 1
    - b: 2

test cmod state module fails:
  cmod.test_succeeded_without_changes:
    - a: 3
    - b: 4
