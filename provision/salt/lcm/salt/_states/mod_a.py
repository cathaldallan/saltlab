"""
This is custom state module A
"""

import logging
LOG = logging.getLogger(__name__)

__virtualname__ = "cmod"

RET = {
    "name": "",
    "changes": {},
    "result": None,
    "comment": ""
}


def __virtual__():        
    if 'local-cm' in __salt__["grains.get"]("roles", []):
        return __virtualname__
    else:
        msg = "Module only available on local-cm configuration managers."
        LOG.debug(msg)
        return False, msg


def test_succeeded(name, a, b):
    ret = dict(RET)
    ret.update({"name": name})

    if __opts__["test"]:        
        ret.update({"comment": "test would succeed with args: {}".format((a, b))})
        return ret
    else:
        ret.update({
            "changes": {
                "old": "The old state of something",
                "new": "The new state of something"
            },
            "result": True,
            "comment": "test succeeded with args: {}".format((a, b))
        })
        return ret

def test_succeeded_without_changes(name, a, b):
    ret = dict(RET)
    ret.update({"name": name})

    if __opts__["test"]:        
        ret.update({"comment": "test would succeed with args: {}".format((a, b))})
        return ret
    else:
        ret.update({            
            "result": True,
            "comment": "test succeeded with args: {}".format((a, b))
        })
        return ret
