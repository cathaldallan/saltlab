"""
This is custom state module B
"""

import logging

__virtualname__ = "cmod"


def __virtual__():    
    if 'central-cm' in __salt__["grains.get"]("roles", []):
        return __virtualname__
    else:
        return False, "Module only available on central-cm configuration managers."
