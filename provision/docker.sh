#!/usr/bin/env bash
apt-get update
apt-get install apt-transport-https ca-certificates curl software-properties-common -y

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

apt-get update
apt-get install docker-ce -y

#docker run --name artifactory -d -p 8081:8081 docker.bintray.io/jfrog/artifactory-pro:latest