hostname=central-cm
# Enable installation of community packages like python-pip
sudo yum -y install https://centos7.iuscommunity.org/ius-release.rpm -y

# Enable installation of the latest salt packages
sudo yum -y install https://repo.saltstack.com/yum/redhat/salt-repo-latest-2.el7.noarch.rpm

# salt installation
yum clean expire-cache
yum install python-pip python36u python36u-pip -y
pip install --upgrade pip
yum install salt-master salt-api salt-minion salt-cloud -y
sleep 5
salt-key -Dy
systemctl stop salt-master salt-api salt-minion

# salt dependencies
pip install CherryPy==3.2.3 --upgrade
pip install pyOpenSSL --upgrade
pip install pyinotify --upgrade

# salt-api setup
salt-call --local tls.create_self_signed_cert
mkdir /var/log/salt/api.d/ -p
id -u saltapiuser &>/dev/null || useradd saltapiuser -m -s /bin/bash -G wheel
echo saltapiuser:abc123 | chpasswd

# salt configuration files
cp -f /vagrant/provision/salt/ccm/conf/master /etc/salt/master
cp -f /vagrant/provision/salt/ccm/conf/minion /etc/salt/minion

# salt keys
mkdir /etc/salt/pki/master/minions/ -p
cp -f /vagrant/provision/salt/keys/master/* /etc/salt/pki/master/
cp -f /vagrant/provision/salt/keys/minions/pub/* /etc/salt/pki/master/minions/
cp -f /vagrant/provision/salt/keys/minions/pub/$hostname /etc/salt/pki/minion/minion.pub
cp -f /vagrant/provision/salt/keys/minions/priv/$hostname.pem /etc/salt/pki/minion/minion.pem

tee /etc/hosts << EOF
127.0.0.1 localhost
::1 ip6-localhost ip6-loopback $hostname
127.0.1.1 $hostname
EOF

systemctl restart salt-master salt-api salt-minion
