#!/usr/bin/env bash
hostname=local-cm
version=2017.7.4
wget -O - https://repo.saltstack.com/apt/ubuntu/16.04/amd64/archive/$version/SALTSTACK-GPG-KEY.pub | sudo apt-key add -
echo "deb http://repo.saltstack.com/apt/ubuntu/16.04/amd64/archive/$version xenial main" > /etc/apt/sources.list.d/saltstack.list

# salt installation
apt update
apt install python-pip -y
pip install --upgrade pip
apt install salt-master salt-api salt-minion salt-syndic -y
sleep 5
salt-key -Dy
systemctl stop salt-master salt-api salt-minion salt-syndic

# salt dependencies
pip install CherryPy==3.2.3 --upgrade
pip install pyOpenSSL --upgrade
pip install pyinotify --upgrade

# salt-api setup
salt-call --local tls.create_self_signed_cert
mkdir /var/log/salt/api.d/ -p
id -u saltapiuser &>/dev/null || useradd saltapiuser -m -s /bin/bash -G sudo
echo saltapiuser:abc123 | chpasswd

# salt configuration files
cp -f /vagrant/provision/salt/lcm/conf/master /etc/salt/master
cp -f /vagrant/provision/salt/lcm/conf/minion /etc/salt/minion

# salt keys
mkdir /etc/salt/pki/master/minions/ -p
cp -f /vagrant/provision/salt/keys/master/* /etc/salt/pki/master/
cp -f /vagrant/provision/salt/keys/minions/pub/$hostname /etc/salt/pki/master/minions/
cp -f /vagrant/provision/salt/keys/minions/pub/$hostname /etc/salt/pki/minion/minion.pub
cp -f /vagrant/provision/salt/keys/minions/priv/$hostname.pem /etc/salt/pki/minion/minion.pem

tee /etc/hosts << EOF
127.0.0.1 localhost
::1 ip6-localhost ip6-loopback $hostname
127.0.1.1 $hostname
EOF

systemctl restart salt-master salt-api salt-minion salt-syndic
