import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot

import pandas
import numpy
import seaborn

# Create barplot
# f, ax = matplotlib.pyplot.subplots(figsize=(15, 6))
matplotlib.pyplot.subplots_adjust(left=0.50)
# matplotlib.pyplot.tight_layout()
seaborn.set_style('whitegrid')
plot_data = {
    'states': [s['id'] for s in stats['states']],
    'run time': [s['duration'] for s in stats['states']]
}
df = pandas.DataFrame(plot_data)
plot = seaborn.barplot(x='run time', y='states', data=plot_data, orient='h')
plot_img = plot.get_figure()

plot_img.savefig("/home/ubuntu/do_plot.png")

